#if (${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ${Name} extends RecyclerView.Adapter<${Name}.${ViewHolder_Name}> {

    private List<${Model_Name}> listItem = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public ${ViewHolder_Name} onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.${Layout_Item_Name}, parent, false);
        return new ${ViewHolder_Name}(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ${ViewHolder_Name} holder, int position) {

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setListItem(List<${Model_Name}> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    public class ${ViewHolder_Name} extends RecyclerView.ViewHolder {
        public ${ViewHolder_Name}(@NonNull View itemView) {
            super(itemView);
        }
    }

    interface OnItemClickListener {
        void onItemClick(${Model_Name} ${Model_Name});
//        void onItemLongClik(Note note);
    }

    public void setOnClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}

